<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('home');
})->name('home')->middleware('guest');

Route::get('orders', function () {
    return view('order');
})->name('order');

Route::get('guest/delivery-zones','GuestController@deliveryZones');
Route::get('guest/delivery-zones/{id}/products','GuestController@productsByDeliveryZoneId');
