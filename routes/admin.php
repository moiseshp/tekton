<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('dashboard', function () {
    return view('admin.dashboard');
})->name('dashboard');

/**
 * Products
 */
Route::get('products-view', function () {
    return view('admin.product');
})->name('products.view');

Route::resource('products','ProductController')->only([
    'index','store','destroy','edit','update'
]);

/**
 * Delivery Zones
 */
Route::get('delivery-zones-view', function () {
    return view('admin.delivery-zones');
})->name('delivery-zones.view');

Route::resource('delivery-zones','DeliveryZoneController')->only([
    'index','store','destroy'
]);
