# Tekton Labs - Prueba Técnica

### Tecnologías
  - Laravel 5.6
  - Vue JS 2.5.7

Repositorio en [Bickbuket](https://bitbucket.org/moiseshp/tekton).

### Instalación

Documentación detallada  para [Laravel 5.6](https://laravel.com/docs/5.6)
**Instalar con composer las Dependencias:**
```sh
$ cd tekton
$ composer update
```

**Permiso para los siguientes directorios**
```sh
$ chmod -R 777 storage
$ chmod -R 777 bootstrap/cache
```
**Key de la Aplicación**
```sh
$ php artisan key:generate
```

###### .ENV
Copiar el archivo **.env.example** y renombrarlo a **.env**

Actualizar la URL del proyecto de acuerdo a su servidor
```sh
$ APP_URL=http://example.io/
```

Configurar la base de datos y sus credenciales para el proyecto en **.env**
```sh
$ DB_DATABASE=example
$ DB_USERNAME=example
$ DB_PASSWORD=example
```
**Base de datos** son creadas con el comando **migrate**:
```sh
$ php artisan migrate
```
##### Para pruebas
Haremos uso de los **seeders**. Antes correr el siguiente comando:
```sh
$ composer dump-autoload
```

**Para crear Usuarios y Productos**:
```sh
$ php artisan db:seed
```
