<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'title' => $faker->text(60),
        'autor' => $faker->name,
        'price' => $faker->randomFloat(6,100,400),
        'discount' => $faker->randomFloat(5,20,50),
        'stock' => $faker->randomNumber(3),
        'state' => $faker->numberBetween(1,3),
    ];
});
