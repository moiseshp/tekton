<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Clase Padre para los modelos
 * @var [type]
 */
abstract class Base extends Model
{
    public function scopeActive($query)
    {
        return $query->where('state',1);
    }

    public function scopeAuthorized($query)
    {
        return $query->where('state','<>',3);
    }
}
