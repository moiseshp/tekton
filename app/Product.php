<?php

namespace App;

class Product extends Base
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'autor', 'price', 'discount', 'stock',
    ];

    /**
     * The delivery zones that belong to the product.
     */
    public function delivery_zones()
    {
        return $this->belongsToMany('App\DeliveryZone');
    }
}
