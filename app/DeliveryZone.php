<?php

namespace App;

class DeliveryZone extends Base
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'polygon', 'name',
    ];

    /**
     * The products that belong to the delivery zone.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
