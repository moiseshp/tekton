<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DeliveryZone;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deliveryZones()
    {
        //  Get all delivery zones
        $zones = DeliveryZone::orderBy('created_at','desc')->get();
        $result = [];
        foreach ($zones as $value) {
            $result[] = [
                'id' => $value->id,
                'name' => $value->name,
                'polygon' => json_decode($value->polygon),
            ];
        }

        return response()->json([
            'result' => $result
        ],200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productsByDeliveryZoneId($id)
    {
        sleep(2);
        //  Get all delivery zones
        $zones = DeliveryZone::find($id);

        return response()->json([
            'result' => $zones->products
        ],200);
    }
}
