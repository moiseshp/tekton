<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DeliveryZone;

class DeliveryZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  Get all delivery zones
        $zones = DeliveryZone::orderBy('created_at','desc')->get();
        $result = [];
        foreach ($zones as $value) {
            $result[] = [
                'id' => $value->id,
                'name' => $value->name,
                'polygon' => json_decode($value->polygon),
            ];
        }

        return response()->json([
            'result' => $result
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  Store new Delivery Zone
        DeliveryZone::create([
            'polygon' => json_encode($request->polygon),
            'name' => 'Zona '.str_random(4)
        ]);

        return response()->json([
            'message' => 'Se creó una Nueva Zona de Reparto',
        ],200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        sleep(1);
        //  Get all product
        $deliveryZone = DeliveryZone::find($id);
        $deliveryZone->delete();
        return response()->json([
            'message' => 'Se eliminó correctamente la Zona de Reparto'
        ],200);
    }

}
