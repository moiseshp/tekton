<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  Get all product
        $data = Product::whereIn('state',[1,2])
                        ->orderBy('created_at','desc')
                        ->get();

        return response()->json([
            'result' => $data
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  Store new product
        $product = Product::create($request->all());
        $product->delivery_zones()->attach($request->deliveryZones);
        //  Store Delivery Zones
        return response()->json(array_merge([
            'message' => 'Se creó un nuevo producto',
        ],$product->toArray()),200);
    }

    /**
     * Edit the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //  Get all product
        $product = Product::find($id);
        $deliveryZones = [
            'deliveryZones' => $product->delivery_zones()->pluck('delivery_zones.id')->toArray()
        ];
        return response()->json(array_merge($product->toArray(),$deliveryZones),200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  Get all product
        $product = Product::find($id);
        $product->update($request->all());
        $product->delivery_zones()->sync($request->deliveryZones);

        return response()->json(array_merge([
            'message' => 'Se actualizó correctamente el producto',
        ],$product->toArray()),200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //  Get all product
        $product = Product::find($id);
        $product->state = 3;
        $product->save();

        return response()->json([
            'message' => 'Se eliminó correctamente el producto'
        ],200);
    }
}
