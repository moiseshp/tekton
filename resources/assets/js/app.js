
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

window.Vue = require('vue')

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/es'
Vue.use(ElementUI, { locale })
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDoYAkg2_d3KrYK9i1ExW7yFseqkSqRHAg',
        libraries: 'places,drawing,geometry' // necessary for places input
    }
})
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//  Auth
Vue.component('login', require('./components/auth/Login.vue'))
Vue.component('member', require('./components/auth/Member.vue'))

//  Order Form
Vue.component('order', require('./components/Order.vue'))

//  Products
Vue.component('product', require('./components/product/Index.vue'))

//  Zone
Vue.component('delivery-zones', require('./components/delivery_zones/Index.vue'))

const app = new Vue({
    el: '#app'
});
