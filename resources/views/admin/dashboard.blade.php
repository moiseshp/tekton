@extends('themes.app')

@section('content')
<section class="container-small text-center dashboard">

    <div class="row">

        <div class="col-sm-6">
            <a href="{{ route('admin.products.view') }}" class="thumbnail">
                <span class="glyphicon glyphicon-book"></span>
                <span class="title">Productos</span>
            </a>
        </div>
        <div class="col-sm-6">
            <a href="{{ route('admin.delivery-zones.view') }}" class="thumbnail">
                <span class="glyphicon glyphicon-map-marker"></span>
                <span class="title">Zonas de Reparto</span>
            </a>
        </div>
    </div>

</section>
@endsection
