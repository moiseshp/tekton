@extends('themes.app')

@section('content')
<ol class="breadcrumb">
    <li><a href="{{ route('admin.dashboard') }}">
        <i class="glyphicon glyphicon-th-large"></i> &nbsp;Dashboard</a></li>
    <li class="active">Zonas de Reparto</li>
</ol>

<delivery-zones/>

@endsection
