
            <footer class="footer-app text-center text-muted small">
                Made with <i class="text-danger glyphicon glyphicon-heart"></i> by Moisés HP
            </footer>

        </div> {{-- Vue --}}
        <!-- librerías js -->
        <!-- Page Specific Scripts -->
        @yield('scripts')
        <!-- end scripts -->
        <script type="text/javascript" src="{{ url( mix('js/app.js') ) }}"></script>
    </body>
</html>
