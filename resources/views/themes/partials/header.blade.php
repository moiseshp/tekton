<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }} &copy; {{ date('Y') }}</title>
    <meta content="width=device-width,initial-scale=0.8,maximum-scale=1.2,minimum-scale=0.6,user-scalable=1" name="viewport"/>
    <meta http-equiv="Content-Language" content="es"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="Distribution" content="global"/>
    <meta name="Robots" content="all"/>
    <style media="screen">
        [v-cloak] > * { display:none }
        [v-cloak]::before {
            content:" ";
            background-color: #FFF;
            background-image: url("/images/loading.gif");
            background-repeat: no-repeat;
            background-position: center center;
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }
    </style>
    <link href="{{ url( mix('css/app.css') ) }}" rel="stylesheet">
    @yield('styles')
</head>
    <body>
        {{-- Vue --}}
        <div id="app" v-cloak>
