@include('themes.partials.header')

<nav class="navbar navbar-default navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <a href="{{ url('/') }}" class="navbar--brand">
            <img
                src="{{ asset('images/tekton-transparent.png') }}"
                class="img-responsive">
        </a>
        <a href="{{ route('home') }}" class="navbar--item pull-left">
            {{ config('app.name') }}
        </a>
        @guest
        <span class="pull-left" style="margin-left:10px">
            <a class="navbar--button" href="{{ route('order') }}">
                <i class="glyphicon glyphicon-tag"></i>&nbsp; Realizar Pedido
            </a>
        </span>
        @endguest

        @auth
            <span class="pull-right">
                <member :user="{{ Auth::user() }}"/>
            </span>
        @else
            <span class="navbar--item pull-right">
            @if ( request()->is('login') )
                <a href="{{ route('home') }}">&larr; Volver</a>
            @else
                <a href="{{ route('login') }}">
                    <i class="glyphicon glyphicon-share"></i>&nbsp; Ingresar
                </a>
            @endif
        </span>
        @endauth

    </div>
</nav>

<div class="container">
    @yield('content')
</div>

@include('themes.partials.footer')
